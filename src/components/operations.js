import { useState } from "react";
import { useRef } from "react";

export default function Button() {
  const opp1 = useRef(null);
  const opp2 = useRef(null);
  const [num, setNum] = useState(0);

  function add(event) {
    const o1 = opp1.current.value;
    const o2 = opp2.current.value;
    const result = parseFloat(o1) + parseFloat(o2);
    setNum(result);
  }

  function subtract(event) {
    const o1 = opp1.current.value;
    const o2 = opp2.current.value;
    const result = parseFloat(o1) - parseFloat(o2);
    setNum(result);
  }

  function multiply(event) {
    const o1 = opp1.current.value;
    const o2 = opp2.current.value;
    const result = parseFloat(o1) * parseFloat(o2);
    setNum(result);
  }

  function divide(event) {
    const o1 = opp1.current.value;
    const o2 = opp2.current.value;
    if (o2 === 0) {
        console.log(event);
        alert("diving by 0 error");
    }
    else{
        const result = parseFloat(o1) / parseFloat(o2);
        setNum(result);
    }
  }

  return (
    <div>
      <div>
        <input name="" ref={opp1}></input>
        <button onClick={add}>+</button>
        <button onClick={subtract}>-</button>
        <button onClick={multiply}>*</button>
        <button onClick={divide}>/</button>
        <input name="" ref={opp2}></input>
      </div>
      <div>
        <h4>Result {num}</h4>
        {/* <label name="result"></label> */}
      </div>
    </div>
  );
}
